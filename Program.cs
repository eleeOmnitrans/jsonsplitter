﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading;


namespace JsonSpliter
{
    class Program
    {
        static void Main(string[] args)
        {
            string jsonfolder = ConfigurationManager.AppSettings["IncomingJsonFolder"];
            string jsonoutputfolder = ConfigurationManager.AppSettings["OutgoingJsonFolder"];
            string jsonArchivefolder = ConfigurationManager.AppSettings["IncomingJsonArchiveFolder"];
            string jsonoutputArchivefolder = ConfigurationManager.AppSettings["OutgoingJsonArchiveFolder"];
            int iCount = 0;
            //string jsonOutputFilename = "";
            string DateFolderName = DateTime.Now.ToString("yyyyMMdd");

            if (!Directory.Exists(jsonArchivefolder + "\\" + DateFolderName))
            {
                Directory.CreateDirectory(jsonArchivefolder + "\\" + DateFolderName);
            }

            if (!Directory.Exists(jsonoutputArchivefolder + "\\" + DateFolderName))
            {
                Directory.CreateDirectory(jsonoutputArchivefolder + "\\" + DateFolderName);
            }

            DirectoryInfo d = new DirectoryInfo(jsonfolder);
            foreach (FileInfo fi in d.GetFiles())
            { 
                if (fi.Extension == ".json")
                {
                    #region Read json file in memory stream
                    StreamReader sr = new StreamReader(fi.FullName);
                    
                    JsonTextReader jtr = new JsonTextReader(sr);
                    JsonSerializer js = new JsonSerializer();
                    StringBuilder sb = new StringBuilder();
                    sb.Clear();

                    while (jtr.Read())
                    {
                        if (jtr.TokenType == JsonToken.StartArray)
                        {
                            string jsonArrayName = jtr.Path;
                            iCount = 0;
                            

                            Console.WriteLine(jsonArrayName);
                            while (jtr.Read())
                            {
                                if (jtr.TokenType == JsonToken.EndArray) {
                                    Console.WriteLine(iCount);

                                    //dump to seperate file
                                    //StreamWriter sw = new StreamWriter(jsonoutputfolder + "\\" + fi.Name.Replace(".json", "") + "_" + jsonArrayName + ".json");
                                    StreamWriter sw = new StreamWriter(jsonoutputfolder + "\\" + jsonArrayName + "_" + fi.Name.Replace(".json", "").Replace("SBShipment_", "") + ".json");
                                    sw.Write("[" + "\n" + sb.ToString() + "\n" + "]");
                                    sw.Close();
                                    sb.Clear();
                                    break; 
                                }
                                if (jtr.TokenType == JsonToken.StartObject)
                                {
                                    var json = js.Deserialize(jtr);
                                    if (iCount == 0)
                                    {
                                        sb.Append(json);
                                    }
                                    else
                                    {
                                        sb.Append(",\n" + json);
                                    }
                                    iCount++;
                                }
                            }

                        }
                    }

                    sr.Close();
                    jtr.Close();
                    #endregion


                }




            }

            #region Archive input and output files

            //Thread.Sleep(1000);

            //File.Move(fi.FullName, jsonArchivefolder + "\\" + DateFolderName + "\\" + fi.Name);
            foreach (var file in Directory.GetFiles(jsonfolder))
            {
                File.Copy(file, Path.Combine(jsonArchivefolder + "\\" + DateFolderName, Path.GetFileName(file)), true);

                File.Delete(file);
            }


            foreach (var file in Directory.GetFiles(jsonoutputfolder))
            {
                File.Copy(file, Path.Combine(jsonoutputArchivefolder + "\\" + DateFolderName, Path.GetFileName(file)), true);
            }

            #endregion
        }
    }
}
